1) Clone the git repository into a local folder.

2) To run the APP, you must rearrange the structure so that the src folder of the project is in the same folder as the executable for the appropriate platform. For example, if running in Windows, you must move the src folder into the nwjs/Windows directory. Then rename the src folder to package.nw.

3) While developping this is not practical for two reasons:
   A) the developper must restore the original structure in order to make a commit.
   B) the RCbenchmark.exe file is the production version of nw.js, and therefore has NO debugging or inspect ability.

Thus to solve these two issues, you can create a Directory Junction link in Windows:

1) Download and extract the development version of NW.js:
YOU MUST USE v0.24 SINCE ITS THE VERSION OUR CUSTOMERS USE!
https://dl.nwjs.io/v0.24.0/nwjs-sdk-v0.24.0-win-x64.zip
Make sure to extract this in another folder separate from the git project.

2) Open CMD as administrator and type:
mklink /J SOURCE TARGET

Example: C:\Users\Dominic Robillard\Desktop\DEV VERSION>mklink /J package.nw "C:\Users\Dominic Robillard\Desktop\git\RCbenchmarkGUI\src"

After the link is created, you will see a "junction" folder in the dev environment. You can edit the code from the git folder directly, but launch the app from the dev executable.
