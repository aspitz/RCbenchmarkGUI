# RCbenchmark.com configuration utility

**Crossplatform control software for the RCbenchmark.com motor test tool**

This GUI is needed to interface with the RCbenchmark.com USB test tool device. With it you can:

-See real-time sensor plots<br />
-Record data to CSV files<br />
-Control your ESC and servo motors<br />
-Upgrade board firmware

[![available on our website](GUI.png)](https://www.rcbenchmark.com)

## Authors

Developped and maintained by Tyto Robotics Inc. This software is written to support the [RCbenchmark.com](http://www.RCbenchmark.com) USB test tool. This test tool was originally a [fork](#credits) of the open source project Cleanflight (see credits below).

## Installation

### Via RCbenchmark website

1. Visit [Our Website](https://www.rcbenchmark.com)
2. Download the software according to your OS

## Credits

Cross Platform Installer:
[![cross platform installer](installersby_tiny.png)](http://www.bitrock.com)

Dominic Clifton/hydra - maintainer of the Cleanflight firmware and configurator, from which this project was forked. Cleanflight Configurator was originally a fork of Baseflight Configurator with support for Cleanflight instead of Baseflight.
Stefan Kolla/cTn-dev - primary author and maintainer of Baseflight Configurator from which Cleanflight was forked.
