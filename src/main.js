'use strict'

function startApplication() {
    var applicationStartTime = new Date().getTime();
    
    chrome.app.window.create('main.html', {
        id: 'main-window',
        frame: 'chrome',
        innerBounds: {
            minWidth: 960,
            minHeight: 650
        }
    }, function (createdWindow) {
        createdWindow.contentWindow.addEventListener('load', function () {
            createdWindow.contentWindow.catch_startup_time(applicationStartTime);
        });
        
        createdWindow.onClosed.addListener(function () {
            // Automatically close the port when application closes
            // Save connectionId in separate variable before createdWindow.contentWindow is destroyed
            var connectionId = createdWindow.contentWindow.serial.connectionId;

            if (connectionId) {
                chrome.serial.disconnect(connectionId, function (result) {
                    console.log('SERIAL: Connection closed - ' + result);
                });
            }
        });
    });
}

chrome.app.runtime.onLaunched.addListener(startApplication);


chrome.notifications.onButtonClicked.addListener(function (notificationId, buttonIndex) {
    if (notificationId == 'baseflight_update') {
        startApplication();
    }
});

// Google Analytics
var googleAnalyticsService = analytics.getService('ice_cream_app');
var googleAnalytics = googleAnalyticsService.getTracker('UA-62215173-1');
var googleAnalyticsConfig = false;
googleAnalyticsService.getConfig().addCallback(function (config) {
    googleAnalyticsConfig = config;
});
//find a place which is persistent
var neverAgain = false;


/*
    If an id is also specified and a window with a matching id has been shown before, the remembered bounds of the window will be used instead.
*/

//Makes links open in external browser
nw.Window.get().on('new-win-policy', function(frame, url, policy) {
  // do not open the window internally
  policy.ignore();
  // and open it in external browser
  nw.Shell.openExternal(url);
});


// shows/hides html elements based on current board version
function showBoardSpecific(newConnection){
    $(".1580").hide();
    $(".1520").hide();
    $(".1780").hide();
    $(".1780PSA").hide();
    $(".1780LCA").hide();
    $(".1780PSB").hide();
    $(".1780LCB").hide();
    $(".1780Aside").hide();
    $(".1780Bside").hide();
    $(".1780Dual").hide();
    if(CONFIG.boardVersion === "Series 1580"){
        $(".1580").show();
    }else if(CONFIG.boardVersion === "Series 1780"){
      $(".1780").show();
      if(CONFIG.s1780detected.PSA) $(".1780PSA").show();
      if(CONFIG.s1780detected.PSB) $(".1780PSB").show();
      if(CONFIG.s1780detected.LCA) $(".1780LCA").show();
      if(CONFIG.s1780detected.LCB) $(".1780LCB").show();
      if(CONFIG.s1780detected.Aside) $(".1780Aside").show();
      if(CONFIG.s1780detected.Bside) $(".1780Bside").show();
      if(CONFIG.s1780detected.Dual) $(".1780Dual").show();
    } else{
        $(".1520").show();
    }
    updateLeftPanelView();
    if(newConnection){
      logSample.refreshHeader();
      logSample.newLog(); 
    }
}

$(document).ready(loadPersistentVariables); //$(document).ready()

function clearAppStorage(){
  chrome.storage.local.clear();
  localStorage.clear();
  setTimeout(function(){
    chrome.runtime.reload();
  }, 1000);
}

function refreshTabsHeight(){
  var height = 236;
  if(activateDevelopperMode.activated)
    height += 26;
  $('#data-pane').css('top', height);
  databaseActive(function(active){
    if(active && CONFIG.database_mode){
      height += 26;
      $('li.tab_database').show(); 
    }else{
      //height += 26;
      $('li.tab_database').hide();
    }
    $('#data-pane').css('top', height);
  });
}

function loadPersistentVariables () {
    translateAll();
    
    // Load from memory
    // See memorymanagement.js. Not necessary to add initial memory load anymore.
    
    for (var attrname in CONFIG) {
        if(typeof CONFIG[attrname] !== "undefined") {
            if (CONFIG[attrname].hasOwnProperty("isStoredVariable")) {
            initialMemoryLoad(function () {}, CONFIG[attrname]);
            }
        }                
    }
        
    selectPlotDefaultVal();  
  
    showBoardSpecific(true);
    
    // alternative - window.navigator.appVersion.match(/Chrome\/([0-9.]*)/)[1];
    GUI.log('Running - OS: <strong>' + GUI.operating_system + '</strong>, ' +
        'Chrome: <strong>' + window.navigator.appVersion.replace(/.*Chrome\/([0-9.]*).*/, "$1") + '</strong>, ' +
        'Configurator: <strong>' + chrome.runtime.getManifest().version + '</strong>');
    
    // show the app version on the GUI, helps when there is customer support to do.
    document.title=chrome.runtime.getManifest().name + " " + chrome.runtime.getManifest().version;

    display_text();

    // log library versions in console to make version tracking easier
    console.log('Libraries: jQuery - ' + $.fn.jquery + ', d3 - ' + d3.version);
	
	//get user limits
	chrome.storage.local.get('USER_LIMITS', function (result) {
		if (result.USER_LIMITS != undefined){
            //copy to overwrite user limits by saved user limits
            for (var attrname in USER_LIMITS) { 
              if(result.USER_LIMITS[attrname]!==undefined){
                USER_LIMITS[attrname] = result.USER_LIMITS[attrname]; 
              }
            }
		}
        
        //get hack codes
        chrome.storage.local.get('HACKS', function (result) {
            HACKS = result.HACKS;
            var str = JSON.stringify(HACKS);
            if(str && str !== "{}")
              GUI.log(str);
            
            refreshCutoffs();
        });
	});

	//get tare values
	chrome.storage.local.get('TARE_OFFSET', function (result) {
		if (result.TARE_OFFSET != undefined){
			TARE_OFFSET = result.TARE_OFFSET;
			LPFLeft.update(-TARE_OFFSET.loadCellLeftResetValue/1000); //zero using tare value
			LPFRight.update(-TARE_OFFSET.loadCellRightResetValue/1000);
			LPFThrust.update(-TARE_OFFSET.loadCellThrustResetValue/1000);
		}	
	});

	//pole number settings
    // getPoles();

	//load scripting setting
	chrome.storage.local.get('SCRIPTING_MODE', function (result) {
		if (result.SCRIPTING_MODE != undefined){
			CONFIG.scripting_mode = result.SCRIPTING_MODE;
		}
		loadScriptingTab();	
        loadDatabaseTab();
	});
	
	//get user display units
	chrome.storage.local.get('UNITS_DISPLAY', function (result) {
		for (var attrname in result.UNITS_DISPLAY) { 
            // Overwrite the property if it existed in memory.
            UNITS.display[attrname] = result.UNITS_DISPLAY[attrname]; 
        }
	});

	refreshCutoffs();
	
	//resize
	function resizeBarCalc(){
      var e = event;  
      function recalc(){
        if(e && e.clientY)
          $('#content').height(e.clientY-22);
        $('#plot').height($(window).height() - $('#content').height() - 47);
        $('#tabDatabase > div > div.lowerScriptContent').height($('#content').height() - $('#tabDatabase > div > div.top').height() - 12);
      }
      setTimeout(recalc,0);
	}	
	
    var resizing = false;
	resizeBarCalc();
	$("#resize-bar").mousedown(recalculateOnResize);
    //$("#resize-bar").resize(recalculateOnResize);
    
    function recalculateOnResize() {
		resizing = true;
		$("#main-wrapper").mousemove(function () {
			if(resizing){
				$("#content, #left-pane").css('pointer-events', 'none');
				$("#main-wrapper").css('cursor', 'row-resize');
				resizeBarCalc();
			}
		});	
		$(window).mouseup(function () {
			$("#content, #left-pane").css('pointer-events', 'auto');
			$("#main-wrapper").css('cursor', 'default');
			resizing = false;
			try{
				//Try to refresh the code editor size if any
				ace.edit("autocontrol-editor").resize(true);
                ace.edit("database-editor").resize(true);
			}catch (e){
			}
		});
	}
        

	//Called when outside of GUI is resized
	$(window).resize(resizeBarCalc);
	
    // Tabs
    var ui_tabs = $('#tabs > ul');
    $('a', ui_tabs).click(function () {
        if ($(this).parent().hasClass('active') == false && !GUI.tab_switch_in_progress) { // only initialize when the tab isn't already active
            var self = this,
                tabClass = $(self).parent().prop('class');

            var tabRequiresConnection = $(self).parent().hasClass('mode-connected');
            
            var tab = tabClass.substring(4);
            var tabName = $(self).text();
            
            if (tabRequiresConnection && !CONFIGURATOR.connectionValid) {
                GUI.log(getMessage('tabSwitchConnectionRequired'));
                return;
            }
            
            if (GUI.connect_lock) { // tab switching disabled while operation is in progress
                GUI.log(getMessage('tabSwitchWaitForOperation'));
                return;
            }
            
            if (GUI.allowedTabs.indexOf(tab) < 0) {
                GUI.log(getMessage('tabSwitchUpgradeRequired', [tabName]));
                return;
            }

            GUI.tab_switch_in_progress = true;

            GUI.tab_switch_cleanup(function () {
                // disable previously active tab highlight
                $('li', ui_tabs).removeClass('active');
                if(tab == 'sensors') {
					$('li.tab_motors').addClass('active');
				}

                // Highlight selected tab
                $(self).parent().addClass('active');

                function content_ready() {
                    GUI.tab_switch_in_progress = false;
                    $("div#tabAutomatic").hide();
                    $("div#tabDatabase").hide();
					$("div#tabContent").show();
                }
                //loads tab content
                if(tab === 'autocontrol'){
					$("div#tabAutomatic").show();
                    $("div#tabDatabase").hide();
					$("div#tabContent").hide();

					TABS[tab].initialize(function(){GUI.tab_switch_in_progress = false;});
                }else{
                    if(tab === 'database'){
                        $("div#tabAutomatic").hide();
                        $("div#tabDatabase").show();
                        $("div#tabContent").hide();

                        TABS[tab].initialize(function(){GUI.tab_switch_in_progress = false;});
                      
                        setTimeout(resizeBarCalc,0);
                    }else{
                        if(tab != 'undefined'){
                            if (tab == 'landing'){
                               TABS.landing.initialize(content_ready);
                                 if($.trim($('#plot').html())==''){
                                     TABS.sensors.initialize(content_ready);
                                 }
                            }else
                                TABS[tab].initialize(content_ready);
                        } else console.log('Tab not found:' + tab);
                    }
                }
            });
        }
    });

    $('#tabs ul.mode-disconnected li a:first').click();
  
    // hide the database tab if the functionality has been removed
    refreshTabsHeight();

    // listen to all input change events and adjust the value within limits if necessary
    $("#content").on('focus', 'input[type="number"]', function () {
        var element = $(this),
            val = element.val();

        if (!isNaN(val)) {
            element.data('previousValue', parseFloat(val));
        }
    });

    $("#content").on('keydown', 'input[type="number"]', function (e) {
        // whitelist all that we need for numeric control
        var whitelist = [
            96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, // numpad and standard number keypad
            109, 189, // minus on numpad and in standard keyboard
            8, 46, 9, // backspace, delete, tab
            190, 110, // decimal point
            37, 38, 39, 40, 13 // arrows and enter
        ];

        if (whitelist.indexOf(e.keyCode) == -1) {
            e.preventDefault();
        }
    });

    $("#content").on('change', 'input[type="number"]', function () {
        var element = $(this),
            min = parseFloat(element.prop('min')),
            max = parseFloat(element.prop('max')),
            step = parseFloat(element.prop('step')),
            val = parseFloat(element.val()),
            decimal_places;

        // only adjust minimal end if bound is set
        if (element.prop('min')) {
            if (val < min) {
                element.val(min);
                val = min;
            }
        }

        // only adjust maximal end if bound is set
        if (element.prop('max')) {
            if (val > max) {
                element.val(max);
                val = max;
            }
        }

        // if entered value is illegal use previous value instead
        if (isNaN(val)) {
            element.val(element.data('previousValue'));
            val = element.data('previousValue');
        }

        // if step is not set or step is int and value is float use previous value instead
        if (isNaN(step) || step % 1 === 0) {
            if (val % 1 !== 0) {
                element.val(element.data('previousValue'));
                val = element.data('previousValue');
            }
        }

        // if step is set and is float and value is int, convert to float, keep decimal places in float according to step *experimental*
        if (!isNaN(step) && step % 1 !== 0) {
            decimal_places = String(step).split('.')[1].length;

            if (val % 1 === 0) {
                element.val(val.toFixed(decimal_places));
            } else if (String(val).split('.')[1].length != decimal_places) {
                element.val(val.toFixed(decimal_places));
            }
        }
    });

	//Keyboard shortcuts
    $('html').on('keydown' , function(event) {
		//Spacebar -> safety cutoff
		if(event.which == 32) {
			userCutoff = true;
			googleAnalytics.sendEvent('SpacebarCutoff', 'Click');
		}
        
        //Notify scripting engine
        if(event.which) {
            scriptSendMessage('keyboardPress', event.which);
        }
    });

    updateWorkingDirectory ();
    
}

function catch_startup_time(startTime) {
    var endTime = new Date().getTime(),
        timeSpent = endTime - startTime;

    googleAnalytics.sendTiming('Load Times', 'Application Startup', timeSpent);
}

function microtime() {
    var now = new Date().getTime() / 1000;

    return now;
}

function millitime() {
    var now = new Date().getTime();

    return now;
}

function bytesToSize(bytes) {
    if (bytes < 1024) {
        bytes = bytes + ' Bytes';
    } else if (bytes < 1048576) {
        bytes = (bytes / 1024).toFixed(3) + ' KB';
    } else if (bytes < 1073741824) {
        bytes = (bytes / 1048576).toFixed(3) + ' MB';
    } else {
        bytes = (bytes / 1073741824).toFixed(3) + ' GB';
    }

    return bytes;
}

Number.prototype.clamp = function(min, max) {
    return Math.min(Math.max(this, min), max);
};

/**
 * String formatting now supports currying (partial application).
 * For a format string with N replacement indices, you can call .format
 * with M <= N arguments. The result is going to be a format string
 * with N-M replacement indices, properly counting from 0 .. N-M.
 * The following Example should explain the usage of partial applied format:
 *  "{0}:{1}:{2}".format("a","b","c") === "{0}:{1}:{2}".format("a","b").format("c")
 *  "{0}:{1}:{2}".format("a").format("b").format("c") === "{0}:{1}:{2}".format("a").format("b", "c")
 **/
String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function (t, i) {
        return args[i] !== void 0 ? args[i] : "{"+(i-args.length)+"}";
    });
};

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// Used to break long text in multiple lines. Useful for tooltips.
function wordWrap(str, maxWidth) {
    function testWhite(x) {
        var white = new RegExp(/^\s$/);
        return white.test(x.charAt(0));
    };
  
    var newLineStr = "\n"; 
    var done = false; 
    var res = '';
    var found;
    do {                    
        found = false;
        // Inserts new line at first whitespace of the line
        for (var i = maxWidth - 1; i >= 0; i--) {
            if (testWhite(str.charAt(i))) {
                res = res + [str.slice(0, i), newLineStr].join('');
                str = str.slice(i + 1);
                found = true;
                break;
            }
        }
        // Inserts new line at maxWidth position, the word is too long to wrap
        if (!found) {
            res += [str.slice(0, maxWidth), newLineStr].join('');
            str = str.slice(maxWidth);
        }

        if (str.length < maxWidth)
            done = true;
    } while (!done);

    return res + str;
}

// ref: http://stackoverflow.com/a/1293163/2343
// This will parse a delimited string into an array of
// arrays. The default delimiter is the comma, but this
// can be overriden in the second argument.
function CSVToArray( strData, strDelimiter ){
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");

    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );


    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];

    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;


    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec( strData )){

        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[ 1 ];

        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (
            strMatchedDelimiter.length &&
            strMatchedDelimiter !== strDelimiter
            ){

            // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push( [] );

        }

        var strMatchedValue;

        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        if (arrMatches[ 2 ]){

            // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            strMatchedValue = arrMatches[ 2 ].replace(
                new RegExp( "\"\"", "g" ),
                "\""
                );

        } else {

            // We found a non-quoted value.
            strMatchedValue = arrMatches[ 3 ];

        }


        // Now that we have our value string, let's add
        // it to the data array.
        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }

    // Return the parsed data.
    return( arrData );
}