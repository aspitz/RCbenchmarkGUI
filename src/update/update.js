var xml = "https://cdn-docs.rcbenchmark.com/software-release/rcb-thrust-stands-01/update.xml"

var xmlhttp = new XMLHttpRequest();
var serVrsnArray;
var currVrsnArray;


var gui = require('nw.gui');
var win = gui.Window.get();
var exec = require('child_process').execFile;




//Allows for the reading of a JSON file from GITHUB server, allowing to read the version of github code. 
xmlhttp.onreadystatechange = function() {    
    
    if (this.readyState == 4 && this.status == 200) {
        var vrsn = this.responseXML.documentElement.innerHTML;
        
        //Used for the update prompt at landing.js
        changeMemory(CONFIG.latest_version, String(vrsn));
        console.log("Website response: latest software version - " + String(vrsn));
	
        //creates arrays out of the versions
        /*serVrsnArray = vrsn.split(".").map(Number);
        currVrsnArray = chrome.runtime.getManifest().version.split(".").map(Number);
        
        checkVersion(serVrsnArray, currVrsnArray);        
        
        if((CONFIG.updateAvailable.value == true) && (CONFIG.updateNotify.value == "1")){
            updateAvailable();
        }
        else{
            var x = document.getElementById('updateDialog');
            x.style.display = 'none';
        }    */
    }
};

xmlhttp.open("GET",xml,true);
xmlhttp.send();

function checkVersion(serVrsnArray, currVrsnArray){

	if(serVrsnArray[0] > currVrsnArray[0]){
	    changeMemory(CONFIG.updateAvailable, true);
	}
	else if(serVrsnArray[1] > currVrsnArray[1]){
	    changeMemory(CONFIG.updateAvailable, true);
	}
	else if(serVrsnArray[2] > currVrsnArray[2]){
	    changeMemory(CONFIG.updateAvailable, true);
	}
	else{
	    changeMemory(CONFIG.updateAvailable, false);
	}
}

function updateAvailable(){
        
    $( function() {
        $("#updateDialog").dialog({
            height: 410,
            width: 700,
            draggable:false,
            modal:true,
            resizable:false,
            dialogClass:'ui-dialog-osx'
        }); 
    }); 

    $(function() { 
        $( "#update").click(function(){ 
		runUpdate();
            $('#updateDialog').dialog("close");
        });
        $( "#dismiss").click(function(){
            $('#updateDialog').dialog("close");
        });
        $( "#neverAgain").click(function(){
            changeMemory(CONFIG.updateNotify, 0);
            $('#updateDialog').dialog("close");
        }); 
    }); 
}

function runUpdate(){
   
    //You can also just distribute the code according to the user's OS
    switch(GUI.operating_system){
        case "Windows":
            gui.Shell.openItem(process.cwd()+'\\update\\autoupdate-windows.exe');
            win.close();
            break;
        case "MacOS":
            //need to find out what activates this           
            gui.Shell.openItem(global.__dirname + '/update/autoupdate-osx.app');
            win.close();                        
            break;
        case "Linux":
		  exec(process.cwd()+'/update/autoupdate-linux-x64.run', function(err, data) {  
			console.log(err)
			console.log(data.toString());                       
		    }); 
            win.close();                        
            break;                       
    }
}
