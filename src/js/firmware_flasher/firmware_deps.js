'use strict';

var SerialPort = require("browser-serialport");
var intel_hex = require('intel-hex');
var Stk500 = require('stk500');

var board = {
    name: "Arduino Uno",
    baud: 57600,
    signature: new Buffer([0x1e, 0x95, 0x0f]),
    pageSize: 128,
    timeout: 400
};

function upload(params, done){
  var path = params.selected_port;
  var data = params.data;
         
  var hex = intel_hex.parse(data).data;
  
  var serialPort = new SerialPort.SerialPort(path, {
    baudrate: board.baud,
  });
  serialPort.on('open', function(){
    Stk500.bootload(serialPort, hex, board, function(error){
      serialPort.close(function (error) {
      	if(error != 'undefined') console.log(error);
      });
      done(error);
    });
  });
}

window.stk500 = {
  upload:upload,
};