'use strict';

var gulp = require('gulp'),
    webpack = require('webpack'),
    del = require('del');

gulp.task('clean', function(cb) {
  del(['build'], cb);
});

gulp.task('bundle', function(cb) {
  webpack({
    target: 'web',
    debug: true,
    bail: true,
    entry: {
      main: './firmware_deps.js'
    },
    output: {
      path: './',
      filename: 'firmware_deps.bundle.js'
    },
    module: {
      loaders: [
        { test: /firmware_deps\.js$/, loader: "transform?brfs" }
      ]
    }
  }, cb);
});

gulp.task('default', ['bundle']);