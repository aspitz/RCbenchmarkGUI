'use strict';
// Log to CSV

var LOGSAMPLE_control = function() {
  this.currentFile = '';
  this.entryValue = 0;
  this.continuousLog = false;
  this.fileWriter = undefined;
  this.buffer = [];
  this.bufferWritingQty = 0;
  this.zeroTime = undefined;
  this.separator = ",";
}

//The header definitions change when units change, and must be reloaded often
LOGSAMPLE_control.prototype.refreshHeader = function(param){
	//header: Displayed in the log file
	//id: to identify this entry
	//[updateFlagFct]: if specified, contains 
	//[debugOnly]: set to true if only recorded to csv when debug is activated
	//working/[display]: two structures (display only if sensor can change unit)
	//    [unit]: the unit text
	//    valueFct: the function returning the value. If display value, the function takes the working value as argument
    //    textFct: the function returning a text (no value). Use either valueFct or textFct, not both
    logSample.LogHeaders = [];
    function addEntry(entry){
        logSample.LogHeaders.push(entry);
    }
    delete logSample.zeroTime;
  
    var us;
    var protocol = TABS.motors.getControlProtocol();
    if(CONTROL_PROTOCOLS[protocol].microseconds){
      us = "µs";
    }
    
    addEntry({
        header: "Time",
        id: "time",
        working: {
          unit: "s",
          valueFct: function() {return logSample.getTimeStamp()}
        }
    });
  
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
      addEntry({
          header: "ESC signal",
          id: "esc",
          working: {
            unit: us,
            valueFct: function() {var i; if(OUTPUT_DATA.active[0]){i = OUTPUT_DATA.ESC_PWM}else{i=''}; return i}
          },
      });

      addEntry({
          header: "Servo 1",
          id: "servo1",
          working: {
            unit: us,
            valueFct: function() {var i; if(OUTPUT_DATA.active[1]){i = OUTPUT_DATA.Servo_PWM[0]}else{i=''}; return i}
          },
      });

      addEntry({
          header: "Servo 2",
          id: "servo2",
          working: {
            unit: us,
            valueFct: function() {var i; if(OUTPUT_DATA.active[2]){i = OUTPUT_DATA.Servo_PWM[1]}else{i=''}; return i}
          },
      });

      addEntry({
          header: "Servo 3",
          id: "servo3",
          working: {
            unit: us,
            valueFct: function() {var i; if(OUTPUT_DATA.active[3]){i = OUTPUT_DATA.Servo_PWM[2]}else{i=''}; return i}
          },
      });
    }  
    var Blabel = " B";
    if(CONFIG.boardVersion === "Series 1780"){
      var Alabel = "";
      if(CONFIG.s1780detected.Bside){
        Alabel = " A";
      }
      addEntry({
          header: "ESC"+Alabel,
          id: "escA",
          working: {
            unit: us,
            valueFct: function() {var i; if(OUTPUT_DATA.active[0]){i = OUTPUT_DATA.ESCA}else{i=''}; return i}
          },
      });

      addEntry({
          header: "Servo"+Alabel,
          id: "servoA",
          working: {
            unit: us,
            valueFct: function() {var i; if(OUTPUT_DATA.active[1]){i = OUTPUT_DATA.ServoA}else{i=''}; return i}
          },
      });
      if(CONFIG.s1780detected.Bside){
        addEntry({
            header: "ESC"+Blabel,
            id: "escB",
            working: {
              unit: us,
              valueFct: function() {var i; if(OUTPUT_DATA.active[2]){i = OUTPUT_DATA.ESCB}else{i=''}; return i}
            },
        });

        addEntry({
            header: "Servo"+Blabel,
            id: "servoB",
            working: {
              unit: us,
              valueFct: function() {var i; if(OUTPUT_DATA.active[3]){i = OUTPUT_DATA.ServoB}else{i=''}; return i}
            },
        });
      }
      
      addEntry({
          header: "Torque"+Alabel,
          id: "torque",
          working: {
            unit: UNITS.text[UNITS.working['torque']],
            valueFct: DATA.getTorque
          },
          display: {
            unit: UNITS.text[UNITS.display['torque']],
            valueFct: function(val) {return UNITS.convertToDisplay('torque', val)}
          },
      });    
    }
    
    if(CONFIG.boardVersion === "Series 1580"){
        addEntry({
            header: "AccX",
            id: "accx",
            updateFlagFct: function() {return FIRMWARE_FLAGS.accelerometerFlag},
            working: {
              unit: "g",
              valueFct: function() {return DATA.getAccelerometer(0)},
            },
        });
        addEntry({
            header: "AccY",
            id: "accy",
            updateFlagFct: function() {return FIRMWARE_FLAGS.accelerometerFlag},
            working: {
              unit: "g",
              valueFct: function() {return DATA.getAccelerometer(1)},
            },
        });
        addEntry({
            header: "AccZ",
            id: "accz",
            updateFlagFct: function() {return FIRMWARE_FLAGS.accelerometerFlag},
            working: {
              unit: "g",
              valueFct: function() {return DATA.getAccelerometer(2)},
            },
        });
        
        addEntry({
            header: "Torque",
            id: "torque",
            updateFlagFct: function() {return FIRMWARE_FLAGS.loadCellLeftFlag || FIRMWARE_FLAGS.loadCellRightFlag},
            working: {
              unit: UNITS.text[UNITS.working['torque']],
              valueFct: DATA.getTorque
            },
            display: {
              unit: UNITS.text[UNITS.display['torque']],
              valueFct: function(val) {return UNITS.convertToDisplay('torque', val)}
            },
        });    
    }   
  
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
      addEntry({
          header: "Thrust",
          id: "thrust",
          updateFlagFct: function() {return FIRMWARE_FLAGS.loadCellThrustFlag},
          working: {
            unit: UNITS.text[UNITS.working['thrust']],
            valueFct: DATA.getThrust
          },
          display: {
            unit: UNITS.text[UNITS.display['thrust']],
            valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
          },
      });

      addEntry({
          header: "Voltage",
          id: "voltage",
          updateFlagFct: function() {return FIRMWARE_FLAGS.escVoltageFlag},
          working: {
            unit: "V",
            valueFct: DATA.getESCVoltage
          },
      });

      addEntry({
          header: "Current",
          id: "current",
          updateFlagFct: function() {return FIRMWARE_FLAGS.escCurrentFlag},
          working: {
            unit: "A",
            valueFct: DATA.getESCCurrent
          },
      });

      addEntry({
          header: "Motor Electrical Speed",
          id: "motorElectricalSpeed",
          updateFlagFct: function() {return FIRMWARE_FLAGS.eRPMFlag},
          working: {
            unit: UNITS.text[UNITS.working['motorSpeed']],
            valueFct: DATA.getElectricalRPM
          },
          display: {
            unit: UNITS.text[UNITS.display['motorSpeed']],
            valueFct: function(val) {return UNITS.convertToDisplay('motorSpeed',val)}
          },
      });

      addEntry({
          header: "Motor Optical Speed",
          id: "motorOpticalSpeed",
          updateFlagFct: function() {return FIRMWARE_FLAGS.oRPMFlag},
          working: {
            unit: UNITS.text[UNITS.working['motorSpeed']],
            valueFct: DATA.getOpticalRPM
          },
          display: {
            unit: UNITS.text[UNITS.display['motorSpeed']],
            valueFct: function(val) {return UNITS.convertToDisplay('motorSpeed',val)}
          },
      });

      addEntry({
          header: "Electrical Power",
          id: "electricalPower",
          updateFlagFct: function() {return FIRMWARE_FLAGS.escVoltageFlag}, // Will update at the same rate as the voltage
          working: {
            unit: "W",
            valueFct: DATA.getElectricalPower
          },
      });
    }  
    if(CONFIG.boardVersion === "Series 1780"){
      addEntry({
          header: "Thrust"+Alabel,
          id: "thrust",
          working: {
            unit: UNITS.text[UNITS.working['thrust']],
            valueFct: DATA.getThrust
          },
          display: {
            unit: UNITS.text[UNITS.display['thrust']],
            valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
          },
      });

      addEntry({
          header: "Voltage"+Alabel,
          id: "voltage",
          working: {
            unit: "V",
            valueFct: DATA.getESCVoltage
          },
      });

      addEntry({
          header: "Current"+Alabel,
          id: "current",
          working: {
            unit: "A",
            valueFct: DATA.getESCCurrent
          },
      });

      addEntry({
          header: "Motor Optical Speed"+Alabel,
          id: "motorOpticalSpeed",
          working: {
            unit: UNITS.text[UNITS.working['motorSpeed']],
            valueFct: DATA.getOpticalRPM
          },
          display: {
            unit: UNITS.text[UNITS.display['motorSpeed']],
            valueFct: function(val) {return UNITS.convertToDisplay('motorSpeed',val)}
          },
      });

      addEntry({
          header: "Electrical Power"+Alabel,
          id: "electricalPower",
          working: {
            unit: "W",
            valueFct: DATA.getElectricalPower
          },
      });
    }
    
    if(CONFIG.boardVersion === "Series 1580"){
        addEntry({
            header: "Mechanical Power",
            id: "mechanicalPower",
            updateFlagFct: function() {return FIRMWARE_FLAGS.loadCellLeftFlag || FIRMWARE_FLAGS.loadCellRightFlag},
            working: {
              unit: "W",
              valueFct: DATA.getMechanicalPower
            },
        });
        
        addEntry({
            header: "Motor Efficiency",
            id: "motorEfficiency",
            updateFlagFct: function() {return FIRMWARE_FLAGS.escVoltageFlag}, // Will update at the same rate as the voltage
            working: {
              unit: "%",
              valueFct: DATA.getMotorEfficiency
            },
        });
        
        addEntry({
            header: "Propeller Mech. Efficiency",
            id: "propMechEfficiency",
            updateFlagFct: function() {return FIRMWARE_FLAGS.loadCellLeftFlag || FIRMWARE_FLAGS.loadCellRightFlag}, // Will update at the same rate as the voltage
            working: {
              unit:  UNITS.text[UNITS.working['thrust']] + "/W",
              valueFct: DATA.getPropMechEfficiency
            },
            display: {
              unit: UNITS.text[UNITS.display['thrust']] + "/W",
              valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
            },
        });
    }
  
    if(CONFIG.boardVersion === "Series 1780"){
        addEntry({
            header: "Mechanical Power"+Alabel,
            id: "mechanicalPower",
            working: {
              unit: "W",
              valueFct: DATA.getMechanicalPower
            },
        });
        
        addEntry({
            header: "Motor Efficiency"+Alabel,
            id: "motorEfficiency",
            working: {
              unit: "%",
              valueFct: DATA.getMotorEfficiency
            },
        });
        
        addEntry({
            header: "Propeller Mech. Efficiency"+Alabel,
            id: "propMechEfficiency",
            working: {
              unit:  UNITS.text[UNITS.working['thrust']] + "/W",
              valueFct: DATA.getPropMechEfficiency
            },
            display: {
              unit: UNITS.text[UNITS.display['thrust']] + "/W",
              valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
            },
        });
    }
  
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
      addEntry({
          header: "Overall Efficiency",
          id: "propElecEfficiency",
          updateFlagFct: function() {return FIRMWARE_FLAGS.loadCellLeftFlag || FIRMWARE_FLAGS.loadCellRightFlag}, // Will update at the same rate as the voltage
          working: {
            unit: UNITS.text[UNITS.working['thrust']] + "/W",
            valueFct: DATA.getPropElecEfficiency
          },
          display: {
            unit: UNITS.text[UNITS.display['thrust']] + "/W",
            valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
          },
      });
    }  
    if(CONFIG.boardVersion === "Series 1780"){
      addEntry({
          header: "Overall Efficiency"+Alabel,
          id: "propElecEfficiency",
          working: {
            unit: UNITS.text[UNITS.working['thrust']] + "/W",
            valueFct: DATA.getPropElecEfficiency
          },
          display: {
            unit: UNITS.text[UNITS.display['thrust']] + "/W",
            valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
          },
      });
    }
  
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1780"){
      if ( DATA.isPressureSensorAvailable() ) {
            addEntry({
                header: "Airspeed",
                id: "airspeed",
                updateFlagFct: function() {return 1},
                working: {
                  unit: "m/s",
                  valueFct: DATA.getAirSpeed
                },
                display: {
                  unit: UNITS.text[UNITS.display['speed']],
                  valueFct: function(val) {return UNITS.convertToDisplay('speed', val)}
                },
            });

            addEntry({
                header: "Airspeed Pressure",
                id: "airspeedPressure",
                updateFlagFct: function() {return 1},
                working: {
                  unit: "Pa",
                  valueFct: DATA.getPressure
                },
                display: {
                  unit: UNITS.text[UNITS.display['pressure']],
                  valueFct: function(val) {return UNITS.convertToDisplay('pressure', val)}
                },
            });
            
            addEntry({
                header: "Electrical forward flight efficiency",
                id: "eForwardFlightEfficiency",
                updateFlagFct: function() {return 1},
                working: {
                  unit: "%",
                  valueFct: DATA.getForwardFlightEfficiency
                },
            });
        
        }
    }
    
    if(CONFIG.boardVersion === "Series 1580" ){      
        addEntry({
            header: "Left Load Cell",
            id: "leftCell",
            debugOnly: true,
            updateFlagFct: function() {return FIRMWARE_FLAGS.loadCellLeftFlag},
            working: {
              unit: "mV",
              valueFct: DATA.getLoadCellLeft
            },
        });
        
        addEntry({
            header: "Right Load Cell",
            id: "rightCell",
            debugOnly: true,
            updateFlagFct: function() {return FIRMWARE_FLAGS.loadCellRightFlag},
            working: {
              unit: "mV",
              valueFct: DATA.getLoadCellRight
            },
        });
    }
  
    if(CONFIG.boardVersion === "Series 1780" ){  
        //Side B data
        if(CONFIG.s1780detected.Bside){
          
          addEntry({
              header: "Torque"+Blabel,
              id: "torqueB",
              working: {
                unit: UNITS.text[UNITS.working['torque']],
                valueFct: function(){return DATA.getTorque(true);}
              },
              display: {
                unit: UNITS.text[UNITS.display['torque']],
                valueFct: function(val) {return UNITS.convertToDisplay('torque', val)}
              },
          });    

          addEntry({
              header: "Thrust"+Blabel,
              id: "thrustB",
              working: {
                unit: UNITS.text[UNITS.working['thrust']],
                valueFct: function(){return DATA.getThrust(true);}
              },
              display: {
                unit: UNITS.text[UNITS.display['thrust']],
                valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
              },
          });

          addEntry({
              header: "Voltage"+Blabel,
              id: "voltageB",
              working: {
                unit: "V",
                valueFct: function(){return DATA.getESCVoltage(true);}
              },
          });

          addEntry({
              header: "Current"+Blabel,
              id: "currentB",
              working: {
                unit: "A",
                valueFct: function(){return DATA.getESCCurrent(true);}
              },
          });

          addEntry({
              header: "Motor Optical Speed"+Blabel,
              id: "motorOpticalSpeedB",
              working: {
                unit: UNITS.text[UNITS.working['motorSpeed']],
                valueFct: function(){return DATA.getOpticalRPM(true);}
              },
              display: {
                unit: UNITS.text[UNITS.display['motorSpeed']],
                valueFct: function(val) {return UNITS.convertToDisplay('motorSpeed',val)}
              },
          });

          addEntry({
              header: "Electrical Power"+Blabel,
              id: "electricalPowerB",
              working: {
                unit: "W",
                valueFct: function(){return DATA.getElectricalPower(true);}
              },
          });

          addEntry({
              header: "Mechanical Power"+Blabel,
              id: "mechanicalPowerB",
              working: {
                unit: "W",
                valueFct: function(){return DATA.getMechanicalPower(true);}
              },
          });

          addEntry({
              header: "Motor Efficiency"+Blabel,
              id: "motorEfficiencyB",
              working: {
                unit: "%",
                valueFct: function(){return DATA.getMotorEfficiency(true);}
              },
          });

          addEntry({
              header: "Propeller Mech. Efficiency"+Blabel,
              id: "propMechEfficiencyB",
              working: {
                unit:  UNITS.text[UNITS.working['thrust']] + "/W",
                valueFct: function(){return DATA.getPropMechEfficiency(true);}
              },
              display: {
                unit: UNITS.text[UNITS.display['thrust']] + "/W",
                valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
              },
          });

          addEntry({
              header: "Overall Efficiency"+Blabel,
              id: "propElecEfficiencyB",
              working: {
                unit: UNITS.text[UNITS.working['thrust']] + "/W",
                valueFct: function(){return DATA.getPropElecEfficiency(true);}
              },
              display: {
                unit: UNITS.text[UNITS.display['thrust']] + "/W",
                valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
              },
          });
          
          //COMBINED
          addEntry({
              header: "Torque A+B",
              id: "torqueDual",
              working: {
                unit: UNITS.text[UNITS.working['torque']],
                valueFct: function(){return DATA.getDualTorque();}
              },
              display: {
                unit: UNITS.text[UNITS.display['torque']],
                valueFct: function(val) {return UNITS.convertToDisplay('torque', val)}
              },
          });    

          addEntry({
              header: "Thrust A+B",
              id: "thrustDual",
              working: {
                unit: UNITS.text[UNITS.working['thrust']],
                valueFct: function(){return DATA.getDualThrust();}
              },
              display: {
                unit: UNITS.text[UNITS.display['thrust']],
                valueFct: function(val) {return UNITS.convertToDisplay('thrust', val)}
              },
          });
          
          addEntry({
              header: "Electrical Power A+B",
              id: "electricalPowerDual",
              working: {
                unit: "W",
                valueFct: function(){return DATA.getDualElectricalPower();}
              },
          });
        }
        for(var i=0;i<6;i++){
          addEntry({
              header: "Load Cell " + i + Alabel,
              id: "loadCell" + i,
              debugOnly: true,
              working: {
                unit: "raw",
                valueFct: (function(i){ return function(){ return SENSOR_DATA.sixAxisForcesRawA[i];}})(i) // needs to be like this because of for loop (https://stackoverflow.com/questions/19696015/
              },
          });
        }
    }
    
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520") addEntry({
        header: "Thrust Load Cell",
        id: "thrustCell",
        debugOnly: true,
        updateFlagFct: function() {return FIRMWARE_FLAGS.loadCellThrustFlag},
        working: {
          unit: "mV",
          valueFct: DATA.getLoadCellThrust
        },
    });
    
    if(CONFIG.boardVersion === "Series 1580") addEntry({
        header: "Vibration",
        id: "accvib",
        updateFlagFct: function() {return FIRMWARE_FLAGS.accelerometerFlag},
        working: {
          unit: "g",
          valueFct: function() {return SENSOR_DATA.vibration},
        },
    });
    
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520") 
      addEntry({
        header: "Burst Current Cutoff",
        id: "currBurst",
        debugOnly: true,
        updateFlagFct: function() {return FIRMWARE_FLAGS.escCurrentFlag},
        working: {
          unit: "A",
          valueFct: function() {return DATA.getESCCurrentBurst()},
        },
    });
  
    if(CONFIG.boardVersion === "Series 1780") 
      addEntry({
        header: "Burst Current Cutoff" + Alabel,
        id: "currBurst",
        debugOnly: true,
        working: {
          unit: "A",
          valueFct: function() {return DATA.getESCCurrentBurst()},
        },
    });
  
    if(CONFIG.s1780detected.Bside){
      for(var i=0;i<6;i++){
        addEntry({
            header: "Load Cell " + i + Blabel,
            id: "loadCellB" + i,
            debugOnly: true,
            working: {
              unit: "raw",
              valueFct: (function(i){ return function(){ return SENSOR_DATA.sixAxisForcesRawB[i];}})(i) // needs to be like this because of for loop (https://stackoverflow.com/questions/19696015/
            },
        });
      }

      addEntry({
        header: "Burst Current Cutoff" + Blabel,
        id: "currBurstB",
        debugOnly: true,
        working: {
          unit: "A",
          valueFct: function() {return DATA.getESCCurrentBurst(true)},
        },
      });
    }
    
    addEntry({
        header: "Pin State 1",
        id: "pinPwm",
        debugOnly: true,
        working: {
          valueFct: function() {return SENSOR_DATA.pinState[0]}
        },
    });
    
    addEntry({
        header: "Pin State 2",
        id: "pinServo1",
        debugOnly: true,
        working: {
          valueFct: function() {return SENSOR_DATA.pinState[1]}
        },
    });
    
    addEntry({
        header: "Pin State 3",
        id: "pinServo2",
        debugOnly: true,
        working: {
          valueFct: function() {return SENSOR_DATA.pinState[2]}
        },
    });
    
    addEntry({
        header: "Pin State 4",
        id: "pinServo3",
        debugOnly: true,
        working: {
          valueFct: function() {return SENSOR_DATA.pinState[3]}
        },
    });
    
    addEntry({
        header: "Debug 0",
        id: "debug0",
        debugOnly: true,
        working: {
          valueFct: function() {return SENSOR_DATA.debug[0]}
        },
    });
    
    addEntry({
        header: "Debug 1",
        id: "debug1",
        debugOnly: true,
        working: {
          valueFct: function() {return SENSOR_DATA.debug[1]}
        },
    });
    
    addEntry({
        header: "Debug 2",
        id: "debug2",
        debugOnly: true,
        working: {
          valueFct: function() {return SENSOR_DATA.debug[2]}
        },
    });
    
    addEntry({
        header: "Debug 3",
        id: "debug3",
        debugOnly: true,
        working: {
          valueFct: function() {return SENSOR_DATA.debug[3]}
        },
    });
    
    var temperatures = DATA.getTemperatures();
    for(var i=0; i<temperatures.length; i++){
        var temp = temperatures[i];
        var getValue = (function(index){
            return function(){
                var temps = DATA.getTemperatures();
                var temp = temps[index];
                var value = 0;
                if(temp===undefined && param!=="noloop"){
                    logSample.refreshHeader("noloop");
                }else{
                    value = temp.value;
                }
                return value;                    
            }
        }(i));
        addEntry({
            header: temp.label + " Temp",
            id: "temp" + temp.id,
            updateFlagFct: function() {return FIRMWARE_FLAGS.temperatureProbes || CONFIG.boardVersion === "Series 1780"}, //for the 1780 we don't use the notion of firmware flags.
            working: {
              unit: UNITS.text[UNITS.working['temperature']],
              valueFct: getValue
            },
            display: {
              unit: UNITS.text[UNITS.display['temperature']],
              valueFct: function(val) {
                  return UNITS.convertToDisplay('temperature', val);
              }
            },
      });
    } 
    
    addEntry({
        header: "App message",
        id: "appMsg",
        working: {
          textFct: logSample.getAppMessage
        },
    });
}

//Called every time new usb data is available
LOGSAMPLE_control.prototype.update = function(){
  if(logSample.continuousLog) logSample.takeSample(1);
  logSample.recordAverage();
}

LOGSAMPLE_control.prototype.takeSample = function(avgQty,callback){
  if(isDirectory()){
    logSample.getReadings(avgQty,function(result){
      var params = {};
      params.callback = callback;
      logSample.recordLine(result, params);
    });
    return true;
  }else return false;
}

function logCallbacksSetup(){
  $('a#newLog.logging').click(function(){
    if(isDirectory()){
      logSample.newLog();
      
      //momentarily flash the new log button to give visual feedback
      $("a#newLog.logging").addClass('ui-block');;
      setTimeout(function() {
        $("a#newLog.logging").removeClass('ui-block');
      }, 40); 
    }
    googleAnalytics.sendEvent('LogNew', 'Click');
  });

  $('input#continousLog.logging').click(function(){
    if(!isDirectory()) $('#continousLog').prop('checked', false);
    logSample.displayContinuousLog();
    googleAnalytics.sendEvent('LogContinuous', 'Click');
  });

  $('a#logButton.logging').click(function () {
    if(logSample.takeSample(5,function(){
      $("a#logButton.logging").removeClass('ui-block');
    })){
      //momentarily flash the take sample button to give visual feedback
      $("a#logButton.logging").addClass('ui-block');
      setTimeout(function() {
        $("a#logButton.logging").removeClass('ui-block');
      }, 40); 
    };
    googleAnalytics.sendEvent('LogSample', 'Click');
  });
}

//Allows appending custom messages to the log
LOGSAMPLE_control.prototype.logAppMessage = function (message){
    logSample.logAppMessage.message = logSample.logAppMessage.message||'';
    logSample.logAppMessage.message += message;
}
LOGSAMPLE_control.prototype.getAppMessage = function (){
    var result = logSample.logAppMessage.message;
    logSample.logAppMessage.message = '';
    var qty = logSample.bufferWritingQty + logSample.buffer.length + logSample.entryValue;
    if(qty===0){
        result = ''; //clears any previous messages when new log file
    }
    return result || '';
}

//Handles the appearance of the continuous log checkbox
LOGSAMPLE_control.prototype.displayContinuousLog = function (){
  logSample.continuousLog = $('#continousLog').is(':checked');
  if(logSample.continuousLog){
    $("#takeSample").css('pointer-events', 'none');
    $( "#newLogFile" ).css('pointer-events', 'none');
    $( "#defaultDirectory" ).css('pointer-events', 'none');
    $("#takeSample").css('opacity', '0.4');
    $( "#newLogFile" ).css('opacity', '0.4');
    $( "#defaultDirectory" ).css('opacity', '0.4');
  }else{
    $("#takeSample").css('pointer-events', '');
    $( "#newLogFile" ).css('pointer-events', '');
    $( "#defaultDirectory" ).css('pointer-events', '');
    $("#takeSample").css('opacity', '1');
    $( "#newLogFile" ).css('opacity', '1');
    $( "#defaultDirectory" ).css('opacity', '1');
  }
}

//Averaging function
LOGSAMPLE_control.prototype.recordAverage = function(){
  if(logSample.LogHeaders===undefined) logSample.refreshHeader();
  if(logSample.averageQty>0 && logSample.resultCallback){    
    //record a sample of every value
    for (var i=0; i<logSample.LogHeaders.length; i++){
      if(logSample.vals[i]===undefined) logSample.vals[i]=[];
      var entry = logSample.LogHeaders[i];
      if(!entry.debugOnly || (entry.debugOnly && CONFIG.developper_mode)){ //Only write debug mode items if debugmode is activated
        var firmwareFlag = true;
        if(entry.updateFlagFct!==undefined) firmwareFlag = entry.updateFlagFct();
        if(firmwareFlag || !logSample.continuousLog) { //Read value if firmware flag or not continuousLog
            if(entry.working.valueFct){
                var valToPush = entry.working.valueFct();
                if(!(valToPush === '')) logSample.vals[i].push(Number(valToPush));   
            }
        } 
      }  
    }

    logSample.averageQty--;
    if(logSample.averageQty===0){
      //take the average of every value and create result structure
      var results = {};
      for (var i=0; i<logSample.LogHeaders.length; i++){
          var entry = logSample.LogHeaders[i];
          if(!entry.debugOnly || (entry.debugOnly && CONFIG.developper_mode)){ //Only write debug mode items if debugmode is activated
            var valAvg = "";
            // if it is text, do not calculate the average value
            if(entry.working.textFct){
                valAvg = entry.working.textFct();
            }else{
                var avgQty = logSample.vals[i].length;
                if(avgQty>0){
                  var sum = 0;
                  logSample.vals[i].forEach(function(val){sum+=val;});
                  valAvg = sum/avgQty;
                }
            }
            var result = {};
            if(entry.id==="motorElectricalSpeed") valAvg = Math.round(valAvg);
            if(entry.id==="motorOpticalSpeed") valAvg = Math.round(valAvg);
            result.workingValue = valAvg;
            result.workingUnit = entry.working.unit;
            result.header = entry.header;
            if(result.workingUnit === undefined) result.workingUnit = '';
            if(entry.display===undefined){
                result.displayValue = result.workingValue;
                result.displayUnit = result.workingUnit;
            }else{
                result.displayValue = entry.display.valueFct(valAvg);
                result.displayUnit = entry.display.unit;
            }
            results[entry.id] = result;
          }
      }

      //return the result
      if(logSample.resultCallback) logSample.resultCallback(results);
    }
  }
}

// returns the log timestamp in seconds
LOGSAMPLE_control.prototype.getTimeStamp = function(){
  var time = window.performance.now();

  if(logSample.zeroTime === undefined){
    logSample.zeroTime = time;
  }

  time = (time-logSample.zeroTime)/1000;
  return time;
}

//records the specified data line to file
//data as returned by the getReadings function
LOGSAMPLE_control.prototype.recordLine = function (data,params){
  var callback = undefined;
  var additionalValues = undefined;
  var rawText = undefined;
  if(params){
    callback = params.callback;
    additionalValues = params.additionalValues;
    rawText = params.rawText;
  }

  var result = true;
  if(isDirectory()){    
    //Check if a file needs to be created
    if(this.fileWriter === undefined){
        this.createFileEntry(recordCSV);
    }else{
      recordCSV();
    }

    //Record the data
    function recordCSV(){
        if(rawText){
          var toWrite = data;
        }else{
          var result = "";
          for (var i=0; i<logSample.LogHeaders.length; i++){
              var entry = logSample.LogHeaders[i];
              if(!entry.debugOnly || (entry.debugOnly && CONFIG.developper_mode)){ //Only write debug mode items if debugmode is activated
                result += data[entry.id].displayValue + logSample.separator;
              }
          }
          if(additionalValues){
            additionalValues.forEach(function(arg){
              result += arg + logSample.separator;
            });
          }
          var toWrite = result +'\n';
        }
        if(logSample.fileWriter.readyState === 2){
          logSample.bufferWritingQty = logSample.buffer.length;
          toWrite += logSample.buffer.join('');
          logSample.buffer = [];

          logSample.fileWriter.onwriteend = function(e) {
            logSample.entryValue += 1 + logSample.bufferWritingQty;
            logSample.bufferWritingQty = 0;
            var num = logSample.updateLogInfo();
            if(callback) callback("Sample#" + num + " saved in " + logSample.currentFile);
          };

          // Create a new Blob and write it to log.txt.
          var bb = new Blob([toWrite], {type: 'text/plain'});
          logSample.fileWriter.write(bb);
        }else{
          // still writing to file from previous iteration
          logSample.buffer.push(toWrite);
          var num = logSample.updateLogInfo();
          if(callback) callback("Sample#" + num + " in write buffer");
        }
    }
  }else{
    result = false;
  }
  return result;
}

//checks if the working directory is set
function isDirectory(){
  var result = true;
  if(CONFIG.workingDirectory === undefined){
    $('#samplesInfo').html('<span style=\"color: red\">'+getMessage("pleaseSetWorkingDirectoryInSetupTab")+'</span>');
    result = false;
    scriptReportError("Cannot create file: the user working directory is not set.");
  }
  return result;
}

//gets the log header
LOGSAMPLE_control.prototype.logHeader = function() {
  if(logSample.noHeader){
    return "";
  }else{
    logSample.refreshHeader();
    // Let excel know that the file is encoded in utf-8 to display µ character
    var header = "\uFEFF";
    
    // Add user notes if any
    if(CONFIG.csvNotes.value && CONFIG.csvNotes.value !== ''){
        header += CONFIG.csvNotes.value + '\n';
    }
    
    // If a control board is used, write some info in the header
    if(CONFIG.controlBoard_flag_active){
      var protocol = TABS.motors.getControlProtocol();
      header += getMessage("controlProtocol") + ": " + getMessage("cb_" + protocol) + '\n';
    }
      
    // Create the header  
    for (var i=0; i<logSample.LogHeaders.length; i++){
      var entry = logSample.LogHeaders[i];
      if(!entry.debugOnly || (entry.debugOnly && CONFIG.developper_mode)){ //Only write debug mode items if debugmode is activated
        header += entry.header;
        var unit = "";
        if(entry.display === undefined){
          if(entry.working.unit) unit = entry.working.unit;
        }else{
          if(entry.display.unit) unit = entry.display.unit;
        }
        if(unit !== "") header += " (" + unit + ")";
        header += logSample.separator; 
      } 
    }
    if(logSample.additionalHeaders){
      logSample.additionalHeaders.forEach(function(arg){
        header += arg + logSample.separator;
      });
    }
    return header;
  }
}

//gets the sensor readings
LOGSAMPLE_control.prototype.getReadings = function(averageQty,resultCallback) {
  logSample.vals = [];
  logSample.averageQty = averageQty;
  logSample.resultCallback = resultCallback;
};

// This method updates the visual log information
LOGSAMPLE_control.prototype.updateLogInfo = function() {
  if(logSample.entryValue>0){
    var text = ' sample';
    var qty = logSample.bufferWritingQty + logSample.buffer.length + logSample.entryValue;
    if(qty>1) text += 's';
    text += ' saved in ';
    $('#samplesInfo').html(qty + text + logSample.currentFile);
  }else{
    $('#samplesInfo').html('');
  }
  return qty;
};

// force a new log file
LOGSAMPLE_control.prototype.newLog = function(params) {
 this.fileWriter = undefined;
 this.entryValue = 0;
 this.zeroTime = undefined;
 this.updateLogInfo();
 this.prefix = undefined;
 this.additionalHeaders = undefined;
 if(params){
   this.prefix = params.prefix;
   this.additionalHeaders = params.additionalHeaders;
   this.noHeader = params.noHeader;
   this.customExtension = params.extension;
 }else{
   this.noHeader = false;
   this.customExtension = undefined;
 }
 logSample.refreshHeader();
};

// This function creates a new file in the default directory and records the header   
LOGSAMPLE_control.prototype.createFileEntry = function (callback){
  // Set the current file name
  var time = new Date();
  if(logSample.prefix === undefined) logSample.prefix = "Log";
  function padDigits(number) {
    return Array(Math.max(2 - String(number).length + 1, 0)).join(0) + number;
  }
  var extension = "";
  if(logSample.customExtension === undefined){
    if(logSample.noHeader){
      extension = "txt";
    }else{
      extension = "csv";
    }
  }else{
    extension = logSample.customExtension;
  }
  var currentFile = logSample.prefix+"_"+time.getFullYear()+'-'+ padDigits(time.getMonth()+1)+'-'+padDigits(time.getDate()) 
                      +'_' + padDigits(time.getHours())+padDigits(time.getMinutes())+ padDigits(time.getSeconds())
                      + "." + extension;
  CONFIG.workingDirectory.getFile(currentFile, {create: true, exclusive: true}, function(fileEntry) {
      //Get a fileWrite for this created file
      fileEntry.createWriter(function(fileWriter) {
            fileWriter.onerror = function(e) {
              logSample.fileWriter.onwriteend = null;
              console.log('Write failed: ' + e.toString());
              scriptReportError('Could not write to log file');
              $('#samplesInfo').html('<span style=\"color: red\">' + getMessage('errorAccessingFile') + '.</span>');
              $('#continousLog').prop('checked', false);
              logSample.bufferWritingQty = 0;
              logSample.displayContinuousLog();
            };

            fileWriter.onwriteend = function(e) {
              if(callback) callback();
            };

            logSample.fileWriter = fileWriter;
            logSample.currentFile = currentFile;

            //Write the header
            var bb = new Blob([logSample.logHeader()+'\n'], {type: 'text/plain'});
            logSample.fileWriter.write(bb); 
       })
  });
};

var logSample = new LOGSAMPLE_control();