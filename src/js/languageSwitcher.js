var dict = {};
var usedTranslations = {}; //useful for cleanup purposes. As a developper, go to visit EVERY error message and feature possible, then call the function generateCleanTranslations() from the console to get the new cleaned-up language files. Also using your editor, find in all files all instances of getMessages and manually call them to ensure the useful ones are not lost. Compare with git is you do a cleanup.

function generateCleanTranslations(){
  function printJSON(langFile){
    var result = {};
    var keys = [];
    for (var key in usedTranslations) {
        keys.push(key);
    }
    keys.sort();
    var result = "{\n";
    keys.forEach(function(key, index){
      if(langFile[key] === undefined){
        alert("Translation was requested but not found for: " + key);
      }else{
        var comma = "";
        if(index+1 < keys.length){
          comma = ",";
        }
        result += '  "' + key + '": ' + JSON.stringify(langFile[key].message) + comma + '\n';
      }
    });
    result += '}';
    
    console.log(result);
  }
  
  $.ajax({
    async: false,
    type: "GET",
    url: "/lang/en.json",
    success: function (msg) {
      printJSON(JSON.parse(msg));
    }
  });
  $.ajax({
    async: false,
    type: "GET",
    url: "/lang/zh.json",
    success: function (msg) {
      printJSON(JSON.parse(msg));
    }
  });
}

function translateAll() {
 
  registerWords();
  //console.log(systemLang,"systemLanguage");
  //console.log(localStorage.getItem('lang'),"locallanguage");
  
  switch(localStorage.getItem("lang")) {
    case "en" :
      setLanguage("en");
      break
    case "zh" :
      setLanguage("zh");
      break
    default:
      setLanguage("en");
  } 
  
	// language switch when you click selectLanguage dropdown list 

	$('#selectlanguage').change(getLanguage);



	function getLanguage() {
	  setLanguage($(this).val());
      $('.tab-autocontrol select.dropdownScripts').html(""); //fix a bug in the autocontrol tab
	  //or handle what is in $(this).val() however you want to
	}
	
}

function setLanguage(lang) {
  translate(lang);
  localStorage.setItem('lang',lang);
}


function translate(lang) {
  if(localStorage.getItem(lang + "Data") != null){
    dict = JSON.parse(localStorage.getItem(lang + "Data"));
  }else{
    loadDict();
  }

  $("[i18n]").each(function () {
    switch (this.tagName.toLowerCase()) {
      case "input":
        $(this).val(getMessage($(this).attr("i18n")));
        break;
      default:
        $(this).html (getMessage($(this).attr("i18n")));
    }
  });
  
}

var parametersForTranslation = {};
function getMessage(src, params) {
  parametersForTranslation[src] = params;
  return buildMessage(src);
}

function buildMessage(src){
	//get message according to key src from dict 
    var message = src;
    if(dict[src] !==undefined){
      message = dict[src];
    }else{
      if(!isEmpty(dict)){
        var lang = (localStorage.getItem("lang") || "en");
        console.error("Missing translation for " + lang + ": " + src); 
        googleAnalytics.sendEvent('MissingTranslation', lang+':'+src);
      }
    }

	//split message according to "$"
	var messageArray = message.split("$");
	var output = "";
	messageArray.forEach(function(elem, index){
		if(index === 0){
			output += elem;
		}else{
			// get variable and index
			var paramIndex = elem.substring(0, 1);
			var paramValue = parametersForTranslation[src][paramIndex-1];
			output += paramValue;
			output += elem.substring(1);
		}
	});
    usedTranslations[src] = true;
	return output;
}

function loadDict(callback) {
  var lang = (localStorage.getItem("lang") || "en");
  $.ajax({
    async: false,
    type: "GET",
    url: "/lang/"+lang + ".json",
    success: function (msg) {
      dict = JSON.parse(msg);
	  if(callback) callback();
    }
  });
}
// loop all i18n to show the translation 
function registerWords() {
  $('[i18n]:not(.i18n-replaced').each(function() {
        var element = $(this);

        element.html(getMessage(element.attr('i18n')));
        element.addClass('i18n-replaced');
		
  });
  
   $('[i18n_title]:not(.i18n_title-replaced').each(function() {
    var element = $(this);

    element.attr('title', getMessage(element.attr('i18n_title')));
    element.addClass('i18n_title-replaced');
  });

  $('[i18n_value]:not(.i18n_value-replaced').each(function() {
    var element = $(this);

    element.val(getMessage(element.attr('i18n_value')));
    element.addClass('i18n_value-replaced');
  });

  $('[i18n_placeholder]:not(.i18n_placeholder-replaced').each(function() {
    var element = $(this);

    element.attr('placeholder', getMessage(element.attr('i18n_placeholder')));
    element.addClass('i18n_placeholder-replaced');
  });
  
  // special case that seems hard to fix (it shows before the translations are loaded?)
  $("#safetyDialog").prop('title', getMessage('safetyAgreement'));
  switch(localStorage.getItem("lang")) {
    case "zh" :
      $("#tabContent .titles").height(40);
      break
    default:
      $("#tabContent .titles").height(20);
  } 
  
}







