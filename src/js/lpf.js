'use strict';

//Constructor. Must specify cutoff frequency during object initialization
var LowPassFilter = function (cutoffFrequencyHz) {
  this.cutoffHz = cutoffFrequencyHz;
  this.filterValue = NaN;
  this.lastSampleTimestamp = window.performance.now();
};

//Call this function when a new sample is received to update the filter.
LowPassFilter.prototype.update = function(newValue) {
  //Calculate the framerate
  var currTimeMs = window.performance.now(); //time in ms
  var dt = 0.001 * (currTimeMs - this.lastSampleTimestamp);
  var fs = 1/dt; //sampling frequency in Hz
  this.lastSampleTimestamp = currTimeMs;

  //Calculate alpha according to Eq.2 of http://www.dsprelated.com/showarticle/182.php
  var eq = Math.cos(2*Math.PI*this.cutoffHz/fs);
  var alpha = eq - 1 + Math.sqrt(eq*eq - 4*eq + 3);

  if(this.cutoffHz === 0 || isNaN(this.filterValue)){
    this.filterValue = newValue;
  }else{
    //Apply the filter according to Fig.1a of http://www.dsprelated.com/showarticle/182.php
    this.filterValue = (1-alpha)*this.filterValue + alpha*newValue;
  }
};

//Returns the latest value of the filter
LowPassFilter.prototype.getValue = function() {
  var returnval = 0;
  if(this && !isNaN(this.filterValue)) returnval = this.filterValue;
  return returnval;
};

//Force the filter to take the next value
LowPassFilter.prototype.forceNextValue = function() {
  this.filterValue = NaN;
}

//Force the filter to take this value now
LowPassFilter.prototype.forceValue = function(forcedValue) {
  this.forceNextValue();
  this.update(forcedValue);
}