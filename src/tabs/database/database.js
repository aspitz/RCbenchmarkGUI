'use strict';

var DB_SCRIPT = {};

var DB_DEV_MODE = false; // for development. Should be false normally. (left section will appear orange, so we don't forget)
var DB_RAW_DATA; // for future database use (includes ALL the sensor readings)
var DB_LOG = '';
var DB_isElectricalRPM;
var DB_fakeDevice;
var DB_TEST_TITLE = "Database test";

// this allows us to discontinue the database feature in the future even for published software (delete the xml file on our server)
function databaseActive(callback){
  /*var databasexml = "https://cdn-docs.rcbenchmark.com/software-release/rcb-thrust-stands-01/database.xml";
  var dbxmlhttp = new XMLHttpRequest();
  dbxmlhttp.open("GET",databasexml,true);
  dbxmlhttp.send();
  dbxmlhttp.onreadystatechange = function() {    
      if (this.readyState == 4 && this.status == 200) {
          var isActive = this.responseXML.documentElement.innerHTML == 1;
          callback(isActive);
      }else{
          callback(false);
      }
  };*/
  
  // From now on the database feature will always be activated.
  callback(true);
  return true;
}

function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
}

function parseScriptParams(values){
  var params = [];
  var script = DB_SCRIPT.scriptNotParsed;
  var outputScript = "";
  
  //start by replacing the filename fields
  var safeFilename = DB_TEST_TITLE.replace(/[/\\?%*:|"<>]/g, '-');
  script = script.split('@@@FILENAME###').join("'" + safeFilename + "'");
  
  
  while(true){
    var start = script.indexOf("@@{");
    var end = script.indexOf("}##");
    
    if(end === -1 || start === -1){
      if(end - start !== 0){
        console.error("Parsing error, opening @@{ not matching with closing }##");
        return;
      }
      outputScript += script; //add the remainder of the script
      return {
        params: params,
        script: outputScript
      };
    }
    if(end<=start){
      console.error("Parsing error, unmatched @@{ and }##");
    }
    
    // extract parameter definition
    var param = JSON.parse(script.substring(start+2, end+1));
    
    // add script before parameter
    outputScript += script.substring(0, start);
    
    // add actual parameter value to the output script
    var val;
    if(values && values[params.length]!==undefined){
      val = values[params.length];
      switch(param.type){
        case "float": case "integer":
          if(val<param.min || val>param.max){
            outputScript += " *** PARAM ERROR: " + val + " should be between " + param.min + " and " + param.max + " *** ";
          }else{
            val = Number(val);
            outputScript += val;
          }
          break;
        case "boolean":
          val = Boolean(val);
          outputScript += val;
          break;
        case "csv":
          outputScript += "[\n";
          var lines = val.split('\n');
          lines.forEach(function(el, index){
              outputScript += "  ["+el+"]";
              if(index < lines.length){
                outputScript += ",";
              }
              outputScript += "\n";
          });
          outputScript += "]";
          break;
        default:
          outputScript += " *** PARSING ERROR: " + param.type + " is an incorrect type *** ";
          break;
      }
    }else{
      outputScript += param.default;
    }
    param.val = val;
    
    // trim script for next iteration
    script = script.substring(end+3);
    
    param.id = "db-param-" + convertToSlug(param.name);    
    params.push(param);
  }
}

function parseAndSaveUserSettings(){
  setTimeout(function(){
    // Extract the values from the GUI
    var values = $(".database-script-user-param").map(function(idx, elem) {
      if($(elem).is(':checkbox')){
        return $(elem)[0].checked;
      }
      return $(elem).val();
    }).get();
    
    // Regenerate the script and it's parameters
    var parsed = parseScriptParams(values);
    DB_SCRIPT.script = parsed.script;
    DB_SCRIPT.editor.setValue(DB_SCRIPT.script,-1);
    
    // Persist the latest settings in local storage
    var scriptName = convertToSlug(DB_SCRIPT.fileName);
    if(DB_SCRIPT_PARAMS === undefined){
      DB_SCRIPT_PARAMS = {};
    }
    if(DB_SCRIPT_PARAMS[scriptName] === undefined){
      DB_SCRIPT_PARAMS[scriptName] = {};
    }
    parsed.params.forEach(function(param){
      DB_SCRIPT_PARAMS[scriptName][param.id] = param.val;
    });
    chrome.storage.local.set({'DB_SCRIPT_PARAMS': DB_SCRIPT_PARAMS});
  },100);
}

//Gets the script parameter value as last set by user, or use default
function getParamVal(param){
  var val = param.default;
  var scriptName = convertToSlug(DB_SCRIPT.fileName);
  if(DB_SCRIPT_PARAMS !== undefined){
    if(DB_SCRIPT_PARAMS[scriptName] !== undefined){
      if(DB_SCRIPT_PARAMS[scriptName][param.id] !== undefined){
        console.log(val);
        val = DB_SCRIPT_PARAMS[scriptName][param.id];
        console.log(val);
      }
    }
  }
  return val;
}

chrome.storage.local.get('DB_TEST_TITLE', function(result){
  if(result.DB_TEST_TITLE)
    DB_TEST_TITLE = result.DB_TEST_TITLE;
});

//Loads the database tab content
function loadDatabaseTab(callback){
  if(CONFIG.database_mode && CONFIG.scripting_mode){
		$('#tabDatabase').load("./tabs/database/database.html", function () {
          DB_SCRIPT.script = "";
          DB_SCRIPT.scriptNotParsed = "";
          DB_SCRIPT.name = "";
          DB_SCRIPT.fileName = "";
          translateAll();
          
          // ensure the developper does not forget to restore to normal before commit
          if(DB_DEV_MODE){
            $("#data-pane").css('background-color', 'orange');
          }
          
          setTimeout(function(){
            //Fill in list of scripts
            TABS.database.fillScriptList();

            function changeScript(){
                var selected_option = String($('.tab-database select.dropdownScripts').val());
                
                function loadScriptViewer(){
                    //Load the script viewer
                    TABS.database.loadBottomFromFiles("./tabs/database/scriptViewer.html","./tabs/database/scriptViewer.css",function(){
                        var editor = ace.edit("database-editor");
                        editor.setTheme("ace/theme/chrome");
                        editor.setShowPrintMargin(false);
                        editor.container.style.opacity=0.7;
                        editor.getSession().setUseWrapMode(true);
                        if(!DB_DEV_MODE)
                          editor.renderer.setShowGutter(false);
                        editor.setReadOnly(true);
                        editor.$blockScrolling = Infinity;
                        editor.getSession().setMode("ace/mode/javascript");	
                        ace.edit("database-editor").resize(true);
                        setTimeout(function(){
                          ace.edit("database-editor").resize(true); // fixes a bug sometimes the editor is not ready
                        },200);
                        DB_SCRIPT.editor = editor;
                      
                        //Handle the test title box
                        $("#database-test-title").keyup(function(){
                          var str = $(this).val().replace(/'/g, '');
                          var trimmedString = str.substring(0, 50);
                          $("#database-test-title").val(str);
                          DB_TEST_TITLE = str;
                          chrome.storage.local.set({'DB_TEST_TITLE': DB_TEST_TITLE});
                          parseAndSaveUserSettings();
                        });
                        $("#database-test-title").val(DB_TEST_TITLE);
                      
                        //Generate user defined parameters section
                        var params = parseScriptParams().params;
                        var paramsView = $("#database-user-params");
                        params.forEach(function(param, index){
                          // print the parameter title
                          paramsView.append('<strong>' + param.name + "</strong>&nbsp;");
                          
                          // add the question mark tooltip
                          var text = "";
                          if(param.hint){
                            text += wordWrap(param.hint + "\n\n", 50);
                          }
                          text += "Default: ";
                          if(param.type === "csv"){
                            text += "\n";
                          }
                          text += param.default;
                          paramsView.append('<img src="images/question.png" title="' + text + '" height="15" width="15"><br/>');
                          
                          // add the form input
                          var val = getParamVal(param);
                          var style = ' style="width: 55px;"';
                          var className = ' class="database-script-user-param"';
                          switch(param.type){
                            case "integer":
                              paramsView.append('<input id="' + param.id + '" type="number" step="1" min="' + param.min + '" max="' + param.max + '" value="' + val + '"' + style + className + '>');
                              break;
                            case "float":
                              paramsView.append('<input id="' + param.id + '" type="number" min="' + param.min + '" max="' + param.max + '" value="' + val + '"' + style + className + '>');
                              break;
                            case "boolean":
                              var checked = val;
                              var checkedStr = "";
                              if(checked){
                                checkedStr = "checked";
                              }
                              paramsView.append('<input type="checkbox" id="' + param.id + '" ' + checkedStr + className + '>');
                              break;
                            case "csv":
                              paramsView.append('<textarea id="' + param.id + '" rows="10" placeholder="' + param.default + '" ' + className + '>' + val + '</textarea>');
                              break;
                          }
                          paramsView.append('<br/><br/>');
                          
                          // monitor for form changes
                          $("#" + param.id).change(function(){
                            parseAndSaveUserSettings();
                          });
                          $("#" + param.id).keyup(function(){
                            parseAndSaveUserSettings();
                          });
                        });
                            
                        parseAndSaveUserSettings();
                        $('#database-run-script').removeClass('ui-block');
                    });
                }

                //Load it from file
                DB_SCRIPT.name = $('.tab-database select.dropdownScripts option:selected').text();
                DB_SCRIPT.fileName = $('.tab-database select.dropdownScripts').val().substring(1);
                console.log()
                $.get('../scripts/database/' + DB_SCRIPT.fileName + '.js', function(data) {
                    DB_SCRIPT.scriptNotParsed = data;
                    
                    //Load the stored user settings
                    chrome.storage.local.get('DB_SCRIPT_PARAMS', function(result){
                      DB_SCRIPT_PARAMS = result.DB_SCRIPT_PARAMS;
                      loadScriptViewer();
                    });
                }, "text");
            }

            //Callback for dropdown change
            $('.tab-database select.dropdownScripts').change(function () {
              $("#no-database-script").hide();
              changeScript();
            });		
            
            //Disable the runscript button
            $('#database-run-script').addClass('ui-block');

            //changeScript();
          }, 0);
            
          //Fill in the run script window
          $('.tab-database div.runScriptWindow').load("./tabs/database/runScript.html",function(){
              translateAll();

              //Apply the css
              $("head").append($("<link rel='stylesheet' href='./tabs/autocontrol/runScript.css' type='text/css' media='screen' />"));

              $('.tab-database #database-start-button').addClass('start-state');
              $('.tab-database #database-start-button').click(function(){
                DB_SCRIPT.postDataA = [];
                DB_SCRIPT.postDataB = [];
                DB_LOG = "";
                DB_isElectricalRPM = undefined;
                runScript(DB_SCRIPT.script, DB_SCRIPT.name);
              });
          });

          //Run script button callback
          TABS.database.runView = false;
          $('.tab-database a.run-button').click(function(){
              //Toggle between views
              if(TABS.database.runView){
                  clearAutoConsole();
                  $('.tab-database select.dropdownScripts').removeClass('ui-block');
                  $('.tab-database div.runScriptWindow').hide();
                  $('.tab-database div.lowerScriptContent').show();
                  TABS.database.runView = false;
                  $('.tab-database #database-run-script').attr('i18n', 'runScriptButton');
              }else{
                  $('.tab-database select.dropdownScripts').addClass('ui-block');
                  $('.tab-database div.runScriptWindow').show();
                  $('.tab-database div.lowerScriptContent').hide();
                  TABS.database.runView = true;
                  $('.tab-database #database-run-script').attr('i18n', 'returnToEdit');
              }
              translateAll();
          });

          if (callback) callback();
		});
    }else{
      if(!CONFIG.scripting_mode){
        $('#tabDatabase').load("./tabs/autocontrol/scriptingDisabled.html", function () {
			translateAll();
			if (callback) callback();
		});
      }else{
    	$('#tabDatabase').load("./tabs/database/databaseDisabled.html", function () {
			translateAll();
			if (callback) callback();
		});
      }
    }
}

// Adds data to the database post
function databaseAdd(data){
  // Ensure there are no hack codes
  var hackstr = JSON.stringify(HACKS);
  if(hackstr && hackstr !== "{}"){
    scriptReportError("Error: hack codes were used. To use the database feature you must reset all hack codes. Activate debug mode from the Setup tab, then go to the debug tab. Type 'reset' in the box.");
  }
  
  // for now we don't support control board for the database
  if(CONFIG.controlBoard_flag_active){
    scriptReportError("Error: at this time we don't yet support the external control board to upload data to our database. Please write us your feedback if you would like this feature to become available.");
  }
    
  // Ensure calibration was done
  if((CONFIG.boardVersion !== 'Series 1780') && (LOAD_CELLS_CALIBRATION.thrustCalibrationInvalid || LOAD_CELLS_CALIBRATION.torqueCalibrationInvalid)){
    scriptReportError("Error: it seems your test tool was not calibrated recently. Please recalibrate before posting to the database.");
  }
  
  function prepareData(result, sideB){
    var rpm;  
    // get the rpm value
    if(sideB){
      // Series 1780 only uses the optical probe, which makes things easier.
      if(result.motorOpticalSpeedB){
        rpm = result.motorOpticalSpeedB.workingValue;
      }
    }else{
      if(result.motorElectricalSpeed){
        if(result.motorElectricalSpeed.workingValue > 0 && result.motorOpticalSpeed.workingValue > 0 && !DB_DEV_MODE){
          scriptReportError("Error: please use only one RPM sensor.");
          return;
        }
        rpm = math.max(result.motorElectricalSpeed.workingValue, result.motorOpticalSpeed.workingValue);
      }else{
        rpm = result.motorOpticalSpeed.workingValue;
      }
      
      // ensure the rpm value is from the same source always
      if(DB_isElectricalRPM === undefined){
        if(result.motorOpticalSpeed.workingValue > 0){
          DB_isElectricalRPM = false;
        }else if (rpm > 0){
          DB_isElectricalRPM = true;
        }
      }else{
        if(DB_isElectricalRPM){
          if(result.motorOpticalSpeed.workingValue > 0){
            scriptReportError("Error: the electrical RPM sensor was used, but rotation on the optical probe was detected. Please only use one.");
          }
        }else{
          if(result.motorElectricalSpeed && result.motorElectricalSpeed.workingValue > 0){
            scriptReportError("Error: the optical RPM sensor was used, but rotation on the electrical probe was detected. Please only use one.");
          }
        }
      }
    }    
    
    var kgf_to_N = 9.80665002864;
    var rpm_to_rad_per_s = 2*math.PI/60.0;
    var datum = {
      time_s: result.time.workingValue
    };
    var device = CONFIG.boardVersion;
    if(data.fakeDevice && DB_DEV_MODE){
      device = data.fakeDevice;
      DB_fakeDevice = device;
    }
    
    // Copy the data in the correct db fields
    datum.rotation_speed_rad_per_s = rpm * rpm_to_rad_per_s;
    if(sideB){
      if(result.escB){
        datum.esc_us = math.round(result.escB.workingValue);
      }
      if(result.torqueB){
        datum.torque_nm = result.torqueB.workingValue;
      }
      if(result.thrustB){
        datum.thrust_n = result.thrustB.workingValue*kgf_to_N;
      }
      if(result.voltageB){
        datum.voltage_v = result.voltageB.workingValue;
      }
      if(result.currentB){
        datum.current_a = result.currentB.workingValue;
      }
    }else{
      datum.thrust_n = result.thrust.workingValue*kgf_to_N;
      datum.voltage_v = result.voltage.workingValue;
      datum.current_a = result.current.workingValue;

      if(device === 'Series 1520' || device === 'Series 1580'){
        datum.esc_us = math.round(result.esc.workingValue);
      }
      if(device === 'Series 1780'){
        datum.esc_us = math.round(result.escA.workingValue);
      }
      if(device === 'Series 1580' || device === 'Series 1780'){
        datum.torque_nm = result.torque.workingValue;
      }
    }
    
    return datum;
  }
  
  var dataA = prepareData(data);
  var dataB = prepareData(data, true);
  
  DB_SCRIPT.postDataA.push(dataA);
  DB_SCRIPT.postDataB.push(dataB);
  
  if(!DB_RAW_DATA){
    DB_RAW_DATA = [];
  }
  DB_RAW_DATA.push(data);
}

function databaseLog(data){
  scriptReportWarning(data);
  DB_LOG += data + '\n';
}

function databasePost(isCoaxial){
  // start by getting the app settings
  chrome.storage.local.get(undefined, function (storageVal) {
    //delete the logDirectory entry (does not make sense to export the user working directory!)
    delete storageVal.logDirectory;
    var appSettings = JSON.stringify(storageVal);
    
    var app_url = "https://database.rcbenchmark.com";
    if(DB_DEV_MODE){
      app_url = "https://rcb-database-staging.herokuapp.com";
      console.log(JSON.stringify(DB_RAW_DATA));
    }
      
    var postData = { 
      device: CONFIG.displayVersion, 
      title: DB_TEST_TITLE.replace(/([^a-z0-9\s\-\"]+)/gi, ''), //this regex matches database validation filter
      data: JSON.stringify(DB_SCRIPT.postDataA),
      raw_data: JSON.stringify(DB_RAW_DATA),
      script: DB_SCRIPT.script,
      script_name: DB_SCRIPT.name,
      is_electrical: (DB_isElectricalRPM ? '1' : '0'),
      magnetic_poles: CONFIG.numberOfMotorPoles.value,
      optical_tapes: CONFIG.numberOfOpticalTape.value,
      app_settings: JSON.stringify(appSettings),
      gui_version: chrome.runtime.getManifest().version,
      firmware_version: CONFIG.firmwareVersion,
      log: DB_LOG
    };
    var device = CONFIG.boardVersion;
    if(DB_fakeDevice && DB_DEV_MODE){
      postData.device = DB_fakeDevice;
    }
    if(isCoaxial){
      if(postData.device !== 'Series 1780'){
        scriptReportError("Coaxial data is only supported on the Series 1780");
        return;
      }
      if(!CONFIG.s1780detected.Full && !DB_DEV_MODE){
        scriptReportError("Cannot submit coaxial data, some components are missing. Ensure you have the load cell and power sensor for both side A and side B.");
        return;
      }
      postData.dataB = JSON.stringify(DB_SCRIPT.postDataB);
    }
    
    // ensure there are at least 2 datapoints
    if(DB_SCRIPT.postDataA.length < 2){
      scriptReportError("There must be at least two data points to upload data: one for zero rpm, and another with data while spinning.");
      return;
    }
    
    if(SCRIPT_ENVIRONMENT === 'database'){
      postData.script_id = $('.tab-database select.dropdownScripts').val().substring(1);
      $.post( app_url + "/tests/gui-post", postData, function(res) {
        if(res.error){
          console.log(res);
          scriptReportError("Database server responded with an error: " + JSON.stringify(res.error));
        }else{
          DB_SCRIPT.postDataA = [];
          DB_SCRIPT.postDataB = [];
          window.open(app_url + res, '_blank');
          scriptSendMessage('databasePosted', app_url + res);
        }
      })
      .fail(function(e) {
        scriptReportError("Database server could not be reached. Please try again later or contact support. Error: " + JSON.stringify(e));
      });
    }else{
      scriptReportWarning("Database functions can only be used in the database tab. No data sent.");
    }
    DB_RAW_DATA = undefined;
    DB_LOG = "";
    DB_fakeDevice = undefined;
    DB_isElectricalRPM = undefined;
  });
}

TABS.database = {};

//Fill in the list of scripts
TABS.database.fillScriptList = function(){
    // Category, Filename (without the ?) and database test_id, Display name
    if(DB_DEV_MODE){
      TABS.database.comboAdd('Development tests','?1580SimulateTest','Simulate 1580 data');
      TABS.database.comboAdd('Development tests','?1780SimulateCoaxialTest','Simulate 1780 coaxial data');
    }
    //TABS.database.comboAdd('Static tests','?StepsV3','Steps v3');
    TABS.database.comboAdd('Static tests','?StepsAutoRangeV3','Steps auto-range v3');
    TABS.database.comboAdd('Static tests','?StepsCoaxialV1','Series 1780 coaxial steps v1');
}

//Loads custom content on the bottom of the scripting tab from files
TABS.database.loadBottomFromFiles = function (htmlFile, cssFile, callback){
	$('.tab-database div.lowerScriptContent').load(htmlFile,function(){
		translateAll();

		//Apply the css
		$("head").append($("<link rel='stylesheet' href='" + cssFile + "' type='text/css' media='screen' />"));
		if(callback) callback();
	});
}

//Adds an option under label
TABS.database.comboAdd = function(label, value, text) {
	if($('.tab-database select.dropdownScripts optGroup[label="' + label + '"]').length===0){
		//Label does not exists, create it first
		$('.tab-database select.dropdownScripts').append('<optgroup label="' + label + '"></optgroup>');
	}
	//Add the option, if it does not already exist
    if($('.tab-database select.dropdownScripts optGroup[label="' + label + '"] option[value="' + value + '"]').length === 0){
        $('.tab-database select.dropdownScripts optGroup[label="' + label + '"]').append('<option value="' + value + '">' + text + '</option>');
    }
    // empty default 
    $('.tab-database select.dropdownScripts').val("");
}

TABS.database.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'database') {
        GUI.active_tab = 'database';
        googleAnalytics.sendAppView('database');
    }
    SCRIPT_ENVIRONMENT = "database";

    if(callback) callback();    
};

TABS.database.cleanup = function (callback) {
    if (callback) callback();
};