'use strict';

TABS.motors = {};
TABS.motors.initialize = function (callback) {
    var self = this;
  
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
      var minCommand = [CONFIG.ESCMin.value, CONFIG.servo1Min.value, CONFIG.servo2Min.value, CONFIG.servo3Min.value];
      var maxCommand = [CONFIG.ESCMax.value, CONFIG.servo1Max.value, CONFIG.servo2Max.value, CONFIG.servo3Max.value];
    }  
    if(CONFIG.boardVersion === "Series 1780"){
      var minCommand = [CONFIG.ESCAMin.value, CONFIG.ServoAMin.value, CONFIG.ESCBMin.value, CONFIG.ServoBMin.value];
      var maxCommand = [CONFIG.ESCAMax.value, CONFIG.ServoAMax.value, CONFIG.ESCBMax.value, CONFIG.ServoBMax.value];
    }

    if (GUI.active_tab != 'motors') {
        GUI.active_tab = 'motors';
        googleAnalytics.sendAppView('Motors');
    }

    $('#tabContent').load("./tabs/motors.html", process_html);
    
    function process_html() {
        // translate to user-selected language
        //localize();
		translateAll();
		if(logSample.continuousLog)
			$('#continousLog').prop('checked', true);
		logCallbacksSetup();
		
        var number_of_valid_outputs = 4;
        if(CONFIG.boardVersion === "Series 1780"){
          number_of_valid_outputs = 2;
          if(CONFIG.s1780detected.Bside){
            number_of_valid_outputs = 4;
          }
        }
      
        function valueActive(index, active){
          if(active){
            $("#content div.values li").eq(index).css('color','black'); 
          }else{
            $("#content div.values li").eq(index).css('color','LightGrey');
          }
        }

        //Set sliders limits and resolution
        var protocol = TABS.motors.getControlProtocol();
        var minPWMTime = CONTROL_PROTOCOLS[protocol].min_val;
        var maxPWMTime = CONTROL_PROTOCOLS[protocol].max_val;
        var step = (maxPWMTime-minPWMTime) / (CONTROL_PROTOCOLS[protocol].msp_max - CONTROL_PROTOCOLS[protocol].msp_min);
        $('#content div.sliders input').each(function (index) {          
            //set sliders values
            $(this).prop('min', minCommand[index]);
            $(this).prop('max', maxCommand[index]);
            $(this).prop('step', step);
        });
      
        //Set starting value
        /*$('#content div.values li:not(:last)').each(function (index) {
            $(this).text(minCommand[index]);
		});*/
        // callback when a slider is moved
        $('#content div.sliders input:not(.master)').on('input', function () {
            var index = $(this).index();
            var val = $(this).val();

            // set the text value under the sliders
            $('#content div.values li').eq(index).text(TABS.motors.pwmPretty(val));

            // save
            if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
              if(index==0) OUTPUT_DATA.ESC_PWM = Number(val);
              if(index>0 && index<number_of_valid_outputs) OUTPUT_DATA.Servo_PWM[index-1] = Number(val);
            }
            if(CONFIG.boardVersion === "Series 1780"){
              if(index==0) OUTPUT_DATA.ESCA = Number(val);
              if(index==1) OUTPUT_DATA.ServoA = Number(val);
              if(index==2) OUTPUT_DATA.ESCB = Number(val);
              if(index==3) OUTPUT_DATA.ServoB = Number(val);
            }
        });
      
        if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
          $('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(1)').html(getMessage("sliderESC"));
		  if(CONFIG.controlBoard_flag_active){
			$('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(2)').html(getMessage("sliderCh1"));
			$('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(3)').html(getMessage("sliderCh2"));
			$('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(4)').html(getMessage("sliderCh3"));
		  }else{
			$('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(2)').html(getMessage("sliderServo1"));
			$('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(3)').html(getMessage("sliderServo2"));
			$('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(4)').html(getMessage("sliderServo3"));
		  }
          
        }  
        if(CONFIG.boardVersion === "Series 1780"){
          if(CONFIG.s1780detected.Bside){
            $('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(1)').html(getMessage("sliderESCA"));
            $('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(2)').html(getMessage("sliderServoA"));
            $('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(3)').html(getMessage("sliderESCB"));
            $('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(4)').html(getMessage("sliderServoB"));
          }else{
            $('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(1)').html(getMessage("sliderESC"));
            $('#tabContent > div.tab-motors > div.motor_testing > div.left > ul > li:nth-child(2)').html(getMessage("sliderServo"));
          }
        }

        // hide sliders not used
        $('#content ul.titles li').slice(number_of_valid_outputs).hide();
        $('#content div.sliders input').slice(number_of_valid_outputs).hide();
        $('#content div.checkboxes input').slice(number_of_valid_outputs).hide();
        $('#content div.values li').slice(number_of_valid_outputs).hide();

        // handle when a slider is actived
        $('#content div.checkboxes input[type="checkbox"]').change(function () {
            var index = $(this).index();
			
			// disactivate safety cutoff (if active) by unchecking sliders
            if(index===0 || (CONFIG.boardVersion === "Series 1780" && index===2)) {
            	safetyCutoffActivated = false;
				userCutoff = false;
            }

            if ($(this).is(':checked')) {
				// enable output
				OUTPUT_DATA.active[index] = 1;
                valueActive(index, true);
				$('#content .tab-motors p.cutoff').hide();
            } else {
                // disable output
                valueActive(index, false);
                OUTPUT_DATA.active[index] = 0;

                // change value to default (for motor only)
                if(CONFIG.boardVersion === "Series 1780"){
                  if(index==0) $('#content div.sliders input').eq(index).val(CONFIG.ESCCutoffA.value); 
                  if(index==2) $('#content div.sliders input').eq(index).val(CONFIG.ESCCutoffB.value); 
                }else{
                  if(index==0) $('#content div.sliders input').eq(index).val(CONFIG.ESCCutoff.value); 
                }

                // trigger change event so value are sent to mcu
                if(index==0) $('#content div.sliders input').eq(index).trigger('input');
            }
        });

        //adjust sliders to current values
        var sliders = $('#content div.sliders input');
        for(var i=0; i<number_of_valid_outputs; i++){
            //Set initial value to match current state
            if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
              if(i==0) sliders.eq(i).val(OUTPUT_DATA.ESC_PWM);
              if(i>0 && i<number_of_valid_outputs)
                sliders.eq(i).val(OUTPUT_DATA.Servo_PWM[i-1]);
            }
            if(CONFIG.boardVersion === "Series 1780"){
              if(i==0) sliders.eq(i).val(OUTPUT_DATA.ESCA);
              if(i==1) sliders.eq(i).val(OUTPUT_DATA.ServoA);
              if(i==2) sliders.eq(i).val(OUTPUT_DATA.ESCB);
              if(i==3) sliders.eq(i).val(OUTPUT_DATA.ServoB);
            }
            $('#content div.checkboxes input').eq(i).attr('checked', OUTPUT_DATA.active[i]!=0);
            valueActive(i, OUTPUT_DATA.active[i]!=0);
        }
        
        $("#cutoffProtocolManualControl").html(getMessage("cb_" + TABS.motors.getControlProtocol()));
      
        // only fire events when all values are set
        sliders.trigger('input');

        // enable all sliders
        sliders.prop('disabled', false);

        logSample.updateLogInfo();
        refreshCutoffs();

        if (callback) callback();
    }
}

/*TABS.motors.controlBoardInit = function (){
  console.log("Initializing control board code");
  if(!CONFIG.controlBoard.value.protocol){
    TABS.motors.controlBoardSet('pwm_50'); //set default protocol to standard PWM (note: protocol constants are defined in the firmware RC_SIGNALS.h)
  }
  
  // TODO: add MSP to check if control board exists or not
  var CBconfig = CONFIG.controlBoard.value;
  //CBconfig.detected = detected;
  changeMemory(CONFIG.controlBoard, CBconfig);
  
  // update the control board after loading settings
  TABS.motors.controlBoardSendMSP();
}*/

// for display, avoid things like 12.67777777777777777777 us
TABS.motors.pwmPretty = function (val){
  return Math.round(val * 100) / 100;
}

TABS.motors.controlBoardSendMSP = function (callback, init){
  if(CONFIG.boardVersion !== "Series 1580")
    return;
  
  var protocol = CONFIG.controlBoard.value.protocol;
  var protocol_id = 0; 
  var pwm_freq = 0;
  
  if(init){
    protocol = 'pwm_50'; //initial scan set to default
  }
  
  switch(protocol) {
    case 'pwm_50':
      protocol_id = 9;
      pwm_freq = 50;
      break;
    case 'pwm_100':
      protocol_id = 9;
      pwm_freq = 100;
      break;
    case 'pwm_200':
      protocol_id = 9;
      pwm_freq = 200;
      break;
    case 'pwm_300':
      protocol_id = 9;
      pwm_freq = 300;
      break;
    case 'pwm_400':
      protocol_id = 9;
      pwm_freq = 400;
      break;
    case 'pwm_500':
      protocol_id = 9;
      pwm_freq = 500;
      break;
    case 'dshot150':
      protocol_id = 4;
      break;
    case 'dshot300':
      protocol_id = 3;
      break;
    case 'dshot600':
      protocol_id = 2;
      break;
    case 'dshot1200':
      protocol_id = 1;
      break;
    case 'dshot300':
      protocol_id = 3;
      break;
    case 'oneshot42':
      protocol_id = 6;
      break;
    case 'oneshot125':
      protocol_id = 7;
      break;
    case 'multishot':
      protocol_id = 8;
      break;
    default:
      console.log("Unknown protocol");
  }
  
  //for safety set defaults
  var minPWMTime = CONTROL_PROTOCOLS[protocol].min_val;
  var maxPWMTime = CONTROL_PROTOCOLS[protocol].max_val;
  var midval = (minPWMTime + maxPWMTime)/2;
  OUTPUT_DATA.active = [0,0,0,0];
  OUTPUT_DATA.ESC_PWM = minPWMTime;
  OUTPUT_DATA.ESCA = minPWMTime;
  OUTPUT_DATA.ESCB = minPWMTime;
  OUTPUT_DATA.Servo_PWM = [midval,midval,midval];
  OUTPUT_DATA.ServoA = midval;
  OUTPUT_DATA.ServoB = midval;

  var freqL = pwm_freq & 0xFF;
  var freqM = (pwm_freq >> 8) & 0xFF;
  var data = [protocol_id, 0x01, freqL, freqM]; //0x01 is the update flag

  MSP.send_message(MSP_codes.MSP_CB_SET, data, false, function(){
    if(callback) callback();
  });
}

TABS.motors.controlBoardSet = function (protocol, callback){
  var oldPro = CONFIG.controlBoard.value.protocol
  CONFIG.controlBoard.value.protocol = protocol;
  changeMemory(CONFIG.controlBoard, CONFIG.controlBoard.value);
  
  //if the protocol changed, set default values
  if(oldPro && (oldPro !== protocol)){
    // reset the log file if protocol is changed. This is because the CSV header mentions the protocol being used.
    setTimeout(function(){
      logSample.refreshHeader();
      logSample.newLog();
      scriptReportWarning("The control protocol was changed. A new log file will be generated.");
    },1);
    
    // default cutoff
    var defaultCutoff = CONTROL_PROTOCOLS[protocol].default_cutoff;
    changeMemory(CONFIG.ESCCutoff, defaultCutoff);
    changeMemory(CONFIG.ESCCutoffA, defaultCutoff);
    changeMemory(CONFIG.ESCCutoffB, defaultCutoff);

    // default min/max values
    var defaultMin = CONTROL_PROTOCOLS[protocol].default_min;
    var defaultMax = CONTROL_PROTOCOLS[protocol].default_max;
    changeMemory(CONFIG.ESCMin, defaultMin);
    changeMemory(CONFIG.ESCMax, defaultMax);
    changeMemory(CONFIG.servo1Min, defaultMin);
    changeMemory(CONFIG.servo1Max, defaultMax);
    changeMemory(CONFIG.servo2Min, defaultMin);
    changeMemory(CONFIG.servo2Max, defaultMax);
    changeMemory(CONFIG.servo3Min, defaultMin);
    changeMemory(CONFIG.servo3Max, defaultMax);
    console.log("Control protocol changed. Setting defaults. " + oldPro + " -> " + protocol);
  }   
  
  TABS.motors.controlBoardSendMSP(callback);
}

TABS.motors.getControlProtocol = function(){
  if(CONFIG.controlBoard_flag_active){
    return CONFIG.controlBoard.value.protocol || 'pwm_50';
  }else{
    return 'pwm_50';
  }
}

TABS.motors.controlBoardInit = function (callback){
  // Do initial scan to see if CB available
  if(CONFIG.boardVersion === "Series 1580"){
    TABS.motors.controlBoardSendMSP(function(){
      // Check if CB detected
      if(CONFIG.controlBoard_flag_active){
        TABS.motors.controlBoardSet(CONFIG.controlBoard.value.protocol, callback); // apply last user selected protocol
      }else{
        TABS.motors.controlBoardSet('pwm_50', callback); // apply last user selected protocol
      }
    }, true); 
  }else{
    TABS.motors.controlBoardSet('pwm_50', callback); // force pwm_50
  }
}

TABS.motors.cleanup = function (callback) {
    if (callback) callback();
};